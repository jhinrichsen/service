// Both a sample for functional options, and a real reusable HTTP client.
// Based on https://commandcenter.blogspot.com/2014/01/self-referential-functions-and-design.html,
// https://dave.cheney.net/2014/10/17/functional-options-for-friendly-apis, and
// https://sagikazarmark.hu/blog/functional-options-on-steroids/.

package service

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

// Service holds coordinates of a remote Service, such as a REST Service.
type Service struct {
	protocol    string
	server      string
	port        int
	contextroot string

	client *http.Client

	// cached instance so we don't need to recalculate it on const values
	baseURL string
}

// Option holds optional parameter for Service.
type Option func(*Service)

// Options turns a list of Option instances into an Option.
func Options(opts ...Option) Option {
	return func(s *Service) {
		for _, opt := range opts {
			opt(s)
		}
	}
}

// Protocol is also known as Scheme of a URL ("http").
func Protocol(s string) Option {
	return func(a *Service) {
		a.protocol = s
	}
}

// Port identifies the remote IP port.
func Port(port int) Option {
	return func(a *Service) {
		a.port = port
	}
}

// Contextroot is the remote context root that many services use, e.g. "api/v4".
func Contextroot(s string) Option {
	return func(a *Service) {
		a.contextroot = s
	}
}

// Client is an internal http client.
// Useful when talking in parallel to multiple services.
func Client(c *http.Client) Option {
	return func(a *Service) {
		a.client = c
	}
}

// New creates an instance of a remote service.
func New(server string, opts ...Option) *Service {
	c := &Service{
		protocol: "https",
		server:   server,
		client:   http.DefaultClient,
	}
	for _, f := range opts {
		f(c)
	}
	c.freeze()
	return c
}

// BaseURL is the first part of the URL for a remote service
func (a *Service) BaseURL() string {
	return a.baseURL
}

// freeze caches the baseURL
func (a *Service) freeze() {
	var sb strings.Builder
	sb.WriteString(fmt.Sprintf("%s://%s", a.protocol, a.server))
	if a.port != 0 {
		sb.WriteString(":")
		sb.WriteString(strconv.Itoa(a.port))
	}
	if len(a.contextroot) > 0 {
		if !strings.HasPrefix(a.contextroot, "/") {
			sb.WriteString("/")
		}
		sb.WriteString(a.contextroot)
	}
	a.baseURL = sb.String()
}
