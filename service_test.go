package service

import (
	"crypto/tls"
	"net/http"
	"testing"
)

const (
	testServer = "localhost"
)

func TestDefault(t *testing.T) {
	c := New(testServer)
	if c.protocol != "https" {
		t.Fatalf("want protocol https but got %q", c.protocol)
	}
	if c.server != testServer {
		t.Fatalf("want server %q but got %q", testServer, c.server)
	}
	if c.port != 0 {
		t.Fatalf("want port 0 but got %d", c.port)
	}
	if len(c.contextroot) > 0 {
		t.Fatalf("want empty contextroot but got %q", c.contextroot)
	}
}

func TestProtocol(t *testing.T) {
	want := "http://localhost"
	got := New(testServer, Protocol("http")).BaseURL()
	if want != got {
		t.Fatalf("want %q but got %q", want, got)
	}
}

func TestPort(t *testing.T) {
	want := "https://localhost:9080"
	got := New(testServer, Port(9080)).BaseURL()
	if want != got {
		t.Fatalf("want %q but got %q", want, got)
	}
}

// TestAggregation simulates varying options from commmandline
func TestAbsoluteContextroot(t *testing.T) {
	want := "https://localhost/api/v4"
	got := New(testServer, Contextroot("/api/v4")).BaseURL()
	if want != got {
		t.Fatalf("want %q but got %q", want, got)
	}
}
func TestRelativeContextroot(t *testing.T) {
	want := "https://localhost/api/v4"
	got := New(testServer, Contextroot("api/v4")).BaseURL()
	if want != got {
		t.Fatalf("want %q but got %q", want, got)
	}
}

func TestWithInsecureClient(t *testing.T) {
	New(testServer, Client(
		&http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					InsecureSkipVerify: true,
				},
			},
		}))
}

func TestPresets(t *testing.T) {
	want := "https://localhost:9080/api/v4"
	presets := Options(Port(9080), Contextroot("/api/v4"))
	got := New(testServer, presets).BaseURL()
	if want != got {
		t.Fatalf("want %q but got %q", want, got)
	}
}
